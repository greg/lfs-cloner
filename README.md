This is a project to test and demo a regression introduced in a recent `git` security which prevents `git clone` from successfully cloning repositories that use LFS.

- https://github.com/git-lfs/git-lfs/issues/5749
- https://lore.kernel.org/git/20240514181641.150112-1-sandals@crustytoothpaste.net/
